# Link Shortner
simple dockerized link shortner using php/symfony, react, mysql 

##requirements:
- Docker and Docker Compose (for Mac and Windows simply install docker desktop) 
- Git

##Setup guide:
clone Project from gitlab repo:
```
git clone https://gitlab.com/behzad.meghrazchi/linkshorner.git
```

cd to project folder and run:
`if it doesn't work try below code with sudo`
```$xslt
docker-composer up -d --build
```

then go to php container by running this command:
```$xslt
docker exec -it php74-container bash
```

go to project folder and run this command:
```$xslt
composer install
composer require asset
```

then go to node container by running this command:
```$xslt
docker exec -it node-container bash
```

run this command:
```$xslt
npm install && npm run dev
```
then go to mysql container by running this command:
```$xslt
docker exec -it mysql8-container sh
```

login to mysql using these credentials with the following command:
```$xslt
username: root
password: secret
mysql -u root -p
```

then create database using this command:
```$xslt
CREATE DATABASE linkshortner CHARACTER SET utf8 COLLATE utf8_general_ci;
```

go to php container and run this command:
```$xslt
docker exec -it php74-container bash
php bin/console doctrine:migrations:migrate
```

now you can access the link shortner on:
```$xslt
http://localhost:8080
```