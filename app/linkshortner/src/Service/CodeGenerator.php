<?php
/**
 * Created by PhpStorm.
 * User: behzad
 * Date: 4/24/21
 * Time: 1:48 AM
 */

namespace App\Service;


use Doctrine\Persistence\ObjectRepository;

class CodeGenerator
{
    private $repository, $characters, $charactersCount, $code, $codeLength, $iterator, $newCode;

    public function __construct()
    {
        $this->characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $this->code = 'Af9';
        $this->codeLength = 0;
        $this->iterator = 1;
    }

    public function generateCode(ObjectRepository $repository)
    {

        $this->repository = $repository;
        $data = $this->repository->findBy(array(),array('id'=>'DESC'),1,0);


        if(sizeof($data) !=0 && !empty($data[0])) {
            $this->code = $data[0]->getCode();
        }

        $this->codeLength = strlen($this->code);
        $this->charactersCount = strlen($this->characters);
        $this->produceCode();
    }

    public function produceCode()
    {

        $char = $this->code[$this->codeLength - $this->iterator];
        $charPos = strpos($this->characters, $char);
        if($char == $this->characters[strlen($this->characters) - 1]) {

            $this->iterator++;
            $this->produceCode();

        } else {

            $newCode = substr_replace($this->code, $this->characters[$charPos+1], $this->codeLength - $this->iterator, 1);

            $iter = $this->iterator;
            while($iter > 1) {
                $newCode = substr_replace($newCode, $this->characters[0], $this->codeLength - $iter + 1, 1);

                $iter--;
            }

            $lastCode = '';
            for ($i=0; $i < $this->codeLength; $i++) {

                $lastCode .= $this->characters[0];
            }

            if($newCode == $lastCode) {

                $newCode =  $this->characters[0];
                for ($i=0; $i <= $this->codeLength - 1; $i++) {

                    $newCode .= $this->characters[0];
                }
            }

            $this->newCode = $newCode;

            return;
        }
    }

    public function getNewCode()
    {
        return $this->newCode;
    }

}