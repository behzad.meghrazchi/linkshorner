<?php

namespace App\LinkShortners;

use App\Util\LinkShortnerInterface;
use GuzzleHttp\Exception\GuzzleException;

class BitlyLinkShortner implements LinkShortnerInterface
{

    public function shorten(string $longUrl): string
    {
        $domain = "bit.ly";

        try {

            $client = new \GuzzleHttp\Client();
            $response = $client->request(
                'POST',
                'https://api-ssl.bitly.com/v4/shorten',
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer 3f110d37add506526fe941a0a92956b70cfdf66d',
                    ],
                    'json' => [
                        "long_url" => $longUrl,
                        "domain" => $domain,
                    ],
                ]
            );

            $result = json_decode($response->getBody()->getContents());

            if (!empty($result) && !empty($result->link)) {

                return $result->link;

            } else {

                return null;
            }

        } catch (GuzzleException $e) {

            return null;
        }
    }
}