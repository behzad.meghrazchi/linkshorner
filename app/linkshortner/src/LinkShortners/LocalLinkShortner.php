<?php

namespace App\LinkShortners;

use App\Entity\URL;

use App\Service\CodeGenerator;
use App\Util\LinkShortnerInterface;
use Doctrine\Persistence\ObjectRepository;

class LocalLinkShortner implements LinkShortnerInterface
{
    private $container, $doctrine;
    public function __construct($container, $doctrine) {

        $this->container = $container;
        $this->doctrine = $doctrine;
    }

    public function shorten(string $longUrl): string {

        $codeGenerator = new CodeGenerator();
        $codeGenerator->generateCode($this->doctrine->getRepository(URL::class));
        $newCode = $codeGenerator->getNewCode();
        $host = $this->container->get('router')->getContext()->getHost();
        $port = $this->container->get('router')->getContext()->getHttpPort();
        $shortUrl = 'http://' . $host . ':' . $port . '/' .$newCode;
        $domain = 'http://' . $host . ':' . $port;

        $url = new URL();
        $url->setLongUrl($longUrl);
        $url->setCode($newCode);
        $url->setShortUrl($shortUrl);
        $url->setDomain($domain);

        $em = $this->doctrine->getManager();
        $em->persist($url);
        $em->flush();

        return $shortUrl;
    }
}