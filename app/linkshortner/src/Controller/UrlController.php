<?php

namespace App\Controller;

use App\LinkShortners\BitlyLinkShortner;
use App\LinkShortners\LocalLinkShortner;
use App\Entity\URL;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UrlController extends AbstractController
{

    /**
     * @Route("/urls", name="url_list", methods={"GET"})
     */
    public function urls() : Response
    {
        $urls = $this->getDoctrine()
            ->getRepository(URL::class)
            ->findAll();

        return $this->json(["urls"=> $urls], Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/urls/shorten", name="shorten_url", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function shortenUrl(Request $request, ValidatorInterface $validator) : Response
    {
        $longUrl = $request->request->get('long_url');
        $type = $request->request->get('type');

        $errors = $this->validateUrl($validator, $longUrl);

        if (0 !== count($errors)) {

            return $this->json(["error" => [
                "message" => $errors[0]->getMessage()
            ]], Response::HTTP_BAD_REQUEST);
        }

        switch($type) {

            case 'internal':
                $linkShortner = new LocalLinkShortner($this->container, $this->getDoctrine());
                break;
            case 'external':
            default:
                $linkShortner = new BitlyLinkShortner();
        }

        $shortUrl = $linkShortner->shorten($longUrl);
        if (empty($shortUrl)) {

            return $this->json([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json([
            "result" => [
                "link" => $shortUrl
            ],
        ], Response::HTTP_ACCEPTED);

    }

    /**
     * @Route("/{code}", name="url_show", methods={"GET"})
     * @param string $code
     * @return Response
     */
    public function handleURL(string $code) : Response
    {
        $url = $this->getDoctrine()
            ->getRepository(URL::class)
            ->findOneBy(array('code' => $code));

        if(!empty($url)) {

            $longUrl = $url->getLongUrl();
            return $this->redirect($longUrl, Response::HTTP_MOVED_PERMANENTLY);

        } else {

            return $this->render('errors/not_found.html.twig');
        }
    }

    /**
     * @param ValidatorInterface $validator
     * @param $longUrl
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function validateUrl(ValidatorInterface $validator, $longUrl)
    {
        $urlNotBlankContraint = new Assert\NotBlank();
        $urlNotBlankContraint->message = 'Url can not be empty';

        $urlFormatConstraint = new Assert\Regex([
            'pattern' => '/^(https?:\\/\\/)?' . // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' . // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' . // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' . // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' . // query string
                '(\\#[-a-z\\d_]*)?$/',
        ]);
        $urlFormatConstraint->message = 'Enter a valid url';

        $errors = $validator->validate($longUrl, [
            $urlNotBlankContraint,
            $urlFormatConstraint
        ]);

        return $errors;
    }
}