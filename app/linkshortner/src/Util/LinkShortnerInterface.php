<?php

namespace App\Util;

interface LinkShortnerInterface {

    function shorten(string $longUrl) : string;

}