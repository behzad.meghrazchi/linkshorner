<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class UrlApiTest extends ApiTestCase
{
    public function testUrlsAPI(): void
    {
        $response = static::createClient()->request('GET', '/urls');
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('urls', $response->toArray());
    }

    public function testUrlsShortenAPI(): void
    {
        $response = static::createClient()->request('POST', 'urls/shorten', [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'json' => [
                "long_url" => 'http://google.com',
                "domain" => 'http://localhost"8080',
                "type" => 'internal',
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('result', $response->toArray());
    }

    public function testHandleUrlAPI(): void
    {
        $response = static::createClient()->request('POST', 'urls/shorten', [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'json' => [
                "long_url" => 'http://google.com',
                "type" => 'internal',
            ]
        ]);

        $data = $response->toArray();
        $short_url = $data['result']['link'];

        $responseHandleUrl = static::createClient()->request('GET', $short_url);
        $this->assertResponseRedirects('http://google.com',Response::HTTP_MOVED_PERMANENTLY);
    }

}
