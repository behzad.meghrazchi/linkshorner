import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'

class App extends React.Component {

    state = {
        long_url: '',
        short_url: '',
        count: 0,
    };

    handleChange = event => {

        this.setState({ long_url: event.target.value });
    };

    handleSubmit = event => {

        event.preventDefault();
        if(!this.validURL(this.state.long_url)) {

            alert('enter a valid url');

        } else {

            axios.post('/urls/shorten', {
                long_url: this.state.long_url,
                type: this.state.count % 3 ? 'external' : 'internal'
            })
            .then(res => {

                let result = res.data.result;
                let short_link = result.link;
                console.log(short_link);

                this.setState({long_url: '', count: ++this.state.count, short_url: short_link});

            }).catch((err, status) => {

                if(status === 400) {

                     alert('enter a valid url');

                 } else if (status === 500) {

                     alert('internal server error')
                 }
            });
        }
    };

    validURL = (str) => {

        let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(str);
    };

    render() {

        return (

            <div>
                <form action="#" onSubmit={this.handleSubmit}>
                    <input type="text" onChange={this.handleChange} value={this.state.long_url} className={"form-control flex"} style={this.longUrlStyle} placeholder={"http://sample.com"} />

                    <input type="submit" className={"btn btn-light-out"} value={"Shorten"} />

                    <div style={this.shortLinkDivStyle}>

                        {
                            this.state.short_url !== ''
                            ? <a href={this.state.short_url} target={"_blank"} style={this.shortLinkStyle}>{ this.state.short_url }</a>
                            : ''
                        }

                    </div>

                </form>
            </div>

        )
    }

    longUrlStyle = {

        color: 'white',
        textTransform: 'none',
        textAlign: 'center',
        marginBottom: '1em'
    };

    shortLinkDivStyle = {

        marginTop: '1em'
    };

    shortLinkStyle = {

        color: '#333',
        fontSize: '24px',
    };

}

if (document.getElementById('root')) {
    ReactDOM.render(<App />, document.getElementById('root'));
}
